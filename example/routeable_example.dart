// Copyright (c) 2014, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart' as io;
import 'package:shelf_route/shelf_route.dart' as r;
import 'package:shelf_exception_response/exception_response.dart';

class MyRouteCreator implements r.Routeable {
  void createRoutes(r.Router router) {
    router..get('/', (_) => new Response.ok("Hello World"))
      ..get('/greeting/{name}', (request) =>
          new Response.ok("Hello ${r.getPathParameter(request, 'name')}"));
  }
}

void main() {
  var router = r.router();

  router.addAll(new MyRouteCreator());

  var handler = const Pipeline()
      .addMiddleware(logRequests())
      .addMiddleware(exceptionResponse())
      .addHandler(router.handler);

  r.printRoutes(router);

  io.serve(handler, 'localhost', 8080).then((server) {
    print('Serving at http://${server.address.host}:${server.port}');
  });
}
