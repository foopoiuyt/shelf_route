// Copyright (c) 2014, The Shelf Route project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_route;

import 'package:shelf/shelf.dart';
import 'package:path/path.dart' as p;
import 'package:matcher/matcher.dart';
import 'src/preconditions.dart';
import 'package:uri/uri.dart';
import 'package:shelf_path/shelf_path.dart';
export 'package:shelf_path/shelf_path.dart' show getPathParameters,
    getPathParameter;



/// A [Handler] that routes to other handlers based on the incoming request
/// (path and method). Note there can be several layers of routers to facilitate
/// modularity.
///
/// When the Router routes a request to a handler it adjusts the requests as follows:
/// * the routes path is removed from the start of the requests pathInfo
/// * the routes path is added to the end of the requests scriptName
///
/// e.g if original request had path info of `/banking' and scriptName was
/// `/abc` then when routing a request of `/banking/accounts` the new request
/// past to the handler will have a pathInfo of `/accounts` and scriptName of
/// `/abc/banking`
///
///
Router router({ HandlerAdapter handlerAdapter: _noopHandlerAdapter,
  PathAdapter pathAdapter: uriTemplatePattern }) =>
    new Router(handlerAdapter: handlerAdapter, pathAdapter: pathAdapter);

@deprecated('use getPathParameters')
Map<String, Object> getPathVariables(Request request) {
  return getPathParameters(request);
}


void printRoutes(Router router) {
  router.fullPaths.forEach((pi) {
    print('${pi.method}\t->\t${pi.path}');
  });
}

final _url = p.url;

class PathInfo {
  final String method;
  final String path;

  PathInfo(String method, String path)
      : this.method = method != null ? method : '*',
          this.path = path != null && !path.isEmpty ? path : '/';
}

typedef Handler HandlerAdapter(Function handler);

Handler _noopHandlerAdapter(Handler handler) =>
    handler;

typedef UriPattern PathAdapter(path);

PathAdapter _wrapPathAdapter(PathAdapter pathAdapter) {
  return (path) {
    ensure(path, anyOf(isNull, new isInstanceOf<String>(),
        new isInstanceOf<UriPattern>()));

    if (path != null && path is UriPattern) {
      return path;
    }

    return pathAdapter(path);
  };

}

UriPattern uriTemplatePattern(path) {

  // Note: we change '/' to '' as UriParser strips trailing '/'
  // and a '' path won't match agains a pattern of '/'
  final nonNullPath = path == null ? '' : path == '/' ? '' : path;
  return new UriParser(new UriTemplate(nonNullPath));
}


class Router {
  final List<_Route> _routes = <_Route>[];
  final _Route _fallbackRoute;

  final HandlerAdapter handlerAdapter;
  final PathAdapter pathAdapter;

  Router._internal(this._fallbackRoute, this.handlerAdapter,
      this.pathAdapter);

  Router({Handler fallbackHandler, this.handlerAdapter: _noopHandlerAdapter,
    PathAdapter pathAdapter: uriTemplatePattern})
      : this.pathAdapter = _wrapPathAdapter(pathAdapter),
        this._fallbackRoute = new _Route(fallbackHandler != null ?
          fallbackHandler : _send404, pathAdapter(null), null, true, null);

  /**
   * Adds a route with the given handler. Either or both [path] and [method]
   * can be null. Null value matches all in that case. So if both are null
   * then all requests are matched.
   *
   * [path] may be either a String or a [UriPattern]. If path is a String it
   * will be parsed as a [UriParser] which means it is expected to conform to
   * a [UriTemplate].
   *
   * If [exactMatch] is true then a path such as `/foo/bar/fum` will not match
   * a path like `/foo/bar`, where as when it is false it will.
   *
   * If [middleware] is included a pipeline will be created with the middleware
   * and [handler]
   */
  @deprecated("use add instead")
  void addRoute(Function handler, {dynamic path, String method,
      bool exactMatch: true, Middleware middleware }) =>
      add(path, [method], handler, exactMatch: exactMatch, middleware: middleware);

  /**
   * Adds a route with the given [handler], [path] and [method].
   *
   * [path] may be either a String or a [UriPattern]. If path is a String it
   * will be parsed as a [UriParser] which means it is expected to conform to
   * a [UriTemplate].
   *
   * The [handler] must be of type [Handler] unless a custom [handlerAdapter]
   * has been set for this router. The [handlerAdapter] will be applied to
   * the provided [handler] to create the actual handler used.
   *
   * If [exactMatch] is true then a path such as `/foo/bar/fum` will not match
   * a path like `/foo/bar`, where as when it is false it will.
   *
   * If [middleware] is included a pipeline will be created with the middleware
   * and [handler]
   */
  void add(dynamic path, List<String> methods, Function handler,
      { bool exactMatch: true, Middleware middleware }) =>
          _routes.add(new _Route(handlerAdapter(handler), pathAdapter(path),
              methods, exactMatch, middleware));

  /// adds all the routes created by [routeable]'s [createRoutes] method
  /// to the current router if [path] is not provided or to a new child [Router]
  /// at [path].
  /// [middleware] is ignored if the [path] is not provided
  void addAll(Routeable routeable,
              { dynamic path, Middleware middleware,
                HandlerAdapter handlerAdapter }) {
    final r = path != null ?
        child(path, middleware: middleware,
            handlerAdapter: handlerAdapter) : this;
    routeable.createRoutes(r);
  }

  /// creates a route with method = GET
  void get(dynamic path, Function handler, { Middleware middleware }) {
    add(path, ['GET'], handler, middleware: middleware);
  }

  /// creates a route with method = POST
  void post(dynamic path, Function handler, { Middleware middleware }) {
    add(path, ['POST'], handler, middleware: middleware);
  }

  /// creates a route with method = PUT
  void put(dynamic path, Function handler, { Middleware middleware }) {
    add(path, ['PUT'], handler, middleware: middleware);
  }

  /// creates a route with method = DELETE
  void delete(dynamic path, Function handler, { Middleware middleware }) {
    add(path, ['DELETE'], handler, middleware: middleware);
  }

  /// adds a child [Router] at [path]
  Router child(dynamic path, { Middleware middleware,
      HandlerAdapter handlerAdapter, PathAdapter pathAdapter }) {
    final ha = handlerAdapter != null ? handlerAdapter : this.handlerAdapter;
    final pa = pathAdapter != null ? _wrapPathAdapter(pathAdapter) :
      this.pathAdapter;

    final child = new Router._internal(_fallbackRoute, ha, pa);
    _routes.add(new _Route.childRouter(child, pa(path), middleware));
    return child;
  }

  @deprecated('use child')
  Router addChildRouter(dynamic path, { Middleware middleware,
      HandlerAdapter handlerAdapter }) =>
          child(path, middleware: middleware, handlerAdapter: handlerAdapter);

  Handler get handler => _handleRequest;

  Iterable<PathInfo> get fullPaths => _createFullPaths([], null);

  Iterable<PathInfo> _createFullPaths(Iterable<String> rootPathSegs,
                                      String method) {
    return _routes.map((r) => r._createFullPaths(rootPathSegs, method))
      .expand((e) => e);
  }


  dynamic _handleRequest(Request request) {
    final route = _routes.firstWhere((r) => r.canHandle(request),
        orElse: () => _fallbackRoute);
    return route.handle(request) ;
  }
}

/// A class capable of creating routes in a given [Router]
abstract class Routeable {
  void createRoutes(Router router);
}

class _Route {
  final Handler _handler;
  final UriPattern _pathPattern;
  final List<String> _methods;
  final bool _exactMatch;
  final Router _childRouter;

  _Route._internal(this._handler, this._pathPattern, this._methods,
      this._exactMatch, this._childRouter) {
    ensure(_handler, isNotNull, 'handler cannot be null');
    ensure(_pathPattern, isNotNull, '_pathPattern cannot be null');
  }

  factory _Route(Handler handler, dynamic path, List<String> methods,
      bool exactMatch, Middleware middleware) {
    return new _Route._internal(_fullHandler(middleware, handler),
        path, methods, exactMatch, null);
  }

  factory _Route.childRouter(Router childRouter, dynamic path,
      Middleware middleware) {
    return new _Route._internal(_fullHandler(middleware, childRouter.handler),
        path, null, false, childRouter);
  }

  static Handler _fullHandler(Middleware middleware, Handler handler) {
    return middleware != null ?
        const Pipeline().addMiddleware(middleware).addHandler(handler) :
          handler;
  }


  bool canHandle(Request request) {
    if (_methods != null && !_methods.contains(request.method)) {
      return false;
    }

    final patternMatches = _pathPattern.matches(request.url);

    if (!patternMatches) {
      return false;
    }

    if (!_exactMatch) {
      return true;
    }

    final uriMatch = _pathPattern.match(request.url);
    final remainingSegments = uriMatch.rest.pathSegments;

    return remainingSegments.length == 0
      || (remainingSegments.length == 1
        && (remainingSegments.first == '' || remainingSegments.first == '/')
    );
  }

  dynamic handle(Request request) {
    // create a new request with the pathInfo and scriptName adjusted relative
    // to route path
    final newRequest = _createNewRequest(request);

    return _handler(newRequest);
  }

  Request _createNewRequest(Request request) {
    final uriMatch = _pathPattern.match(request.url);

    final requestSegs = request.url.pathSegments;

    final skipCount = request.scriptName.isNotEmpty && requestSegs.isNotEmpty
        && requestSegs.first == '/' ? 1 : 0;
    final remainingPath = _cleanRest(uriMatch.rest);

    final extraScriptPath = requestSegs.skip(skipCount).take(
        requestSegs.length - remainingPath.pathSegments.length);
    var newScriptName = _adjustPath(_url.joinAll(_url.split(request.scriptName)
        ..addAll(extraScriptPath)));

    var newPathInfo = _adjustPath(remainingPath.toString());

    if (newPathInfo == request.url.toString()) {
      newPathInfo = '';
    }

    if (newPathInfo == '' && newScriptName == '') {
      newPathInfo = '/';
    }

    final newUrl = Uri.parse(newPathInfo);

    var newContext = request.context;
    if (uriMatch.parameters.isNotEmpty) {
//      final currentPathVariables = getPathParameters(request);
//      final newPathVariables = {}..addAll(currentPathVariables)
//          ..addAll(uriMatch.parameters);

      newContext = {}..addAll(request.context);
      addPathParametersToContext(newContext, uriMatch.parameters);
    }

    return request.change(context: newContext, url: newUrl,
        scriptName: newScriptName);

  }

  // workaround for https://github.com/google/uri.dart/issues/16
  Uri _cleanRest(Uri rest) {
    if (rest.hasAuthority || rest.fragment == '' || rest.query == '') {
      final builder = new UriBuilder.fromUri(rest);
      if (builder.fragment == '') {
        builder.fragment = null;
      }
      if (builder.host == '') {
        builder.host = null;
      }
      if (builder.queryParameters != null && builder.queryParameters.isEmpty) {
        builder.queryParameters = null;
      }

      return builder.build();
    }
    return rest;
  }

  String _adjustPath(String path) {
    if (path == '/') {
      return '';
    }
    if (path.isNotEmpty && !path.startsWith('/') && !path.startsWith('?')) {
      return _url.join('/', path);
    }

    return path;
  }

  Iterable<PathInfo> _createFullPaths(Iterable<String> rootPathSegs,
                                      String method) {
    final routeSegsStr = (_pathPattern is UriParser)
      ? (_pathPattern as UriParser).template.template
      : "......";

    // remove '/' unless first segment
    final relRouteSegsStr = rootPathSegs.isEmpty ? routeSegsStr :
      _relative(routeSegsStr);


    final newPathSegs = new List()..addAll(rootPathSegs)..add(relRouteSegsStr);

    if (_childRouter != null) {
      return _childRouter._createFullPaths(newPathSegs, method);
    }
    else {
      final path = (_url.joinAll(newPathSegs) as String)
          .replaceAll('/{?', '{?'); // ugly

      final _newMethods = method != null ? [method] :
        _methods != null ? _methods : ['*'];
      return _newMethods.map((m) => new PathInfo(m, path));
    }

  }

  String _relative(String routeSegsStr) {
//    _url.relative(routeSegsStr, from: '/');
    final sep = _url.separator;
    return routeSegsStr.startsWith(sep) ? routeSegsStr.substring(1)
        : routeSegsStr;
  }

  String toString() =>
      '$_methods>$_pathPattern ${_exactMatch ? '(exact)' : ''}';

}

Response _send404(Request req) {
  return new Response.notFound("Not Found");
}


