## 0.6.0
Major release with lots of changes including:

* custom path adapters
  * Allows you to plugin any path style you like (e.g. `/:foo`)
* custom handler adapters
  * Seamless integration with other Shelf components like Shelf Bind so you can use ordinary Dart functions as Shelf Handlers. 
  For example:  
```
  var myRouter = router(handlerAdapter: bind.handlerAdapter)
  myRouter.get('/{name}', (name) => "Hello ${name}");
```
* Support for Shelf Path to remove direct dependency of handlers to Shelf Route
* Easy to add per route middleware
* print all routes in a router
* control over exact path matching
* Fixed path handling on windows


## 0.5.0

* Upgraded to shelf 0.5.1. Note this is only a breaking change for those that
directly accessed the path variables in the header. Unfortunately the example
and doco did this so I made this a major release 

## 0.4.0

* Upgraded to shelf 0.5.0

## 0.3.1

* Support for custom [UriPattern](http://pub.dartlang.org/packages/uri)'s 

## 0.3.0

* Use [UriPattern and UriTemplate](http://pub.dartlang.org/packages/uri) 
instead of custom path matching

    * path variables now use `{}` instead of `:`

## 0.2.0+1

* Fixed handling of routes that are just a path variable
 
## 0.2.0

* Upgraded to shelf 0.4.0 

## 0.1.2+1

* Fixed bug where body was missing on copied request 

## 0.1.2

* Added helper function `getPathVariables` to encapsulate extracting path 
 variables stored in the request

## 0.1.1

* Added support for path variables.

* Improved tests.
